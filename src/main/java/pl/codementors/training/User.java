package pl.codementors.training;


public class User {

    private String login;
    private String password;

    public static void main(String[] args) {

        User userWithWrongPasswd = new User("Karol","123");
        User userWithCorrectPasswd = new User("Maja","123456789");

        userWithWrongPasswd.printUser();
        System.out.println();
        userWithCorrectPasswd.printUser();


        //TODO
        //Jak jest bardziej prawidłowo?
        //userWithCorrectPasswd.printUser() czy printUser(userWithCorrectPasswd);

    }

    public User(String login,String password){
        this.login = login;
        this.password = password;
    }

    public void printUser(){
        System.out.println("Login: " + login);
        if (password.length()<8){
            System.out.println("Hasło: błędne hasło");
        }
        else System.out.println("Hasło: " + drawStars(password.length()));

    }

    public static String drawStars(int stars){
        String starsToReturn ="";

        for (int i=1;i<=stars;i++){
            starsToReturn += "*";
        }
    return starsToReturn;

    }

}
